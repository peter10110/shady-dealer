[1mdiff --git a/sd/Assets/SD/Prefabs/HumanTemplate.prefab b/sd/Assets/SD/Prefabs/HumanTemplate.prefab[m
[1mindex 3a62b55..c83e6f6 100644[m
[1m--- a/sd/Assets/SD/Prefabs/HumanTemplate.prefab[m
[1m+++ b/sd/Assets/SD/Prefabs/HumanTemplate.prefab[m
[36m@@ -24,7 +24,7 @@[m [mGameObject:[m
   - 136: {fileID: 136000012776912178}[m
   m_Layer: 0[m
   m_Name: HumanTemplate[m
[31m-  m_TagString: Untagged[m
[32m+[m[32m  m_TagString: HumanTemplate[m
   m_Icon: {fileID: 0}[m
   m_NavMeshLayer: 0[m
   m_StaticEditorFlags: 0[m
[36m@@ -86,7 +86,7 @@[m [mMeshRenderer:[m
   m_LightProbeUsage: 1[m
   m_ReflectionProbeUsage: 1[m
   m_Materials:[m
[31m-  - {fileID: 2100000, guid: 76ff537c8e1a84345868e6aeee938ab3, type: 2}[m
[32m+[m[32m  - {fileID: 2100000, guid: ab4cca42a576a55458f5ff61f7093673, type: 2}[m
   m_SubsetIndices: [m
   m_StaticBatchRoot: {fileID: 0}[m
   m_ProbeAnchor: {fileID: 0}[m
[36m@@ -179,8 +179,12 @@[m [mMonoBehaviour:[m
     m_PostInfinity: 2[m
     m_RotationOrder: 0[m
   movingTowards: 0[m
[32m+[m[32m  movementDistance: 0[m
[32m+[m[32m  currentDistance: 0[m
   movementDirection: {x: 0, y: 0, z: 0}[m
   rBody: {fileID: 54000014268643950}[m
[32m+[m[32m  model: {fileID: 0}[m
[32m+[m[32m  modelStartOffset: {x: 0, y: 0, z: 0}[m
 --- !u!136 &136000012776912178[m
 CapsuleCollider:[m
   m_ObjectHideFlags: 1[m
[1mdiff --git a/sd/Assets/SD/Scenes/HumanTest.unity.meta b/sd/Assets/SD/Scenes/HumanTest.unity.meta[m
[1mindex f2682fa..8a228b3 100644[m
[1m--- a/sd/Assets/SD/Scenes/HumanTest.unity.meta[m
[1m+++ b/sd/Assets/SD/Scenes/HumanTest.unity.meta[m
[36m@@ -1,5 +1,5 @@[m
 fileFormatVersion: 2[m
[31m-guid: 6bde38e9fc5ad9b4e8cc5204aae21229[m
[32m+[m[32mguid: cf5d6afea9493e043a0ba69d5c6a3d14[m
 timeCreated: 1475311089[m
 licenseType: Free[m
 DefaultImporter:[m
[1mdiff --git a/sd/Assets/SD/Scripts/People/Human.cs b/sd/Assets/SD/Scripts/People/Human.cs[m
[1mindex 7ea68d4..2300bb2 100644[m
[1m--- a/sd/Assets/SD/Scripts/People/Human.cs[m
[1m+++ b/sd/Assets/SD/Scripts/People/Human.cs[m
[36m@@ -24,10 +24,11 @@[m [mpublic class Human : MonoBehaviour {[m
     }[m
 [m
 	// Use this for initialization[m
[31m-	void Start () {[m
[31m-        movingTowards = true;[m
[32m+[m	[32mvoid Start () {[m[41m[m
[32m+[m[32m        /**movingTowards = true;[m
         movementDirection = (targetIntersection.gameObject.transform.position - this.gameObject.transform.position).normalized;[m
         movementDistance = (targetIntersection.Position - this.gameObject.transform.position).magnitude;[m
[32m+[m[32m        */[m[41m[m
         modelStartOffset = model.transform.localPosition;[m
     }[m
 [m
[1mdiff --git a/sd/ProjectSettings/TagManager.asset b/sd/ProjectSettings/TagManager.asset[m
[1mindex 69fdb48..ca6b925 100644[m
[1m--- a/sd/ProjectSettings/TagManager.asset[m
[1m+++ b/sd/ProjectSettings/TagManager.asset[m
[36m@@ -5,6 +5,7 @@[m [mTagManager:[m
   serializedVersion: 2[m
   tags:[m
   - Intersection[m
[32m+[m[32m  - HumanTemplate[m
   layers:[m
   - Default[m
   - TransparentFX[m
