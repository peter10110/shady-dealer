﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CameraController : MonoBehaviour
{

    public List<GameObject> levels = new List<GameObject>();
    public float animTime = 5f;
    public AnimationCurve animPosCurve;
    public AnimationCurve animRotationCurve;
    private int startIndex = 0;

    void Start()
    {
        while (GameController.currentlevelNum < GameController.targetlevelNum)
        {
            GameController.Instance.NextLevel(true);
        }
        StopAllCoroutines();
        startLevel(GameController.currentlevelNum,true);

    }
    void Update()
    {
        
        if(Input.GetKeyUp(KeyCode.F1))
        {
            GameController.Instance.PrevLevel();
        }
        else if (Input.GetKeyUp(KeyCode.F2))
        {
            GameController.Instance.NextLevel();
        }
        else if (Input.GetKeyUp(KeyCode.F3))
        {
            StopAllCoroutines();
            GameController.Instance.PrevLevel(true);
        }
        else if (Input.GetKeyUp(KeyCode.F4))
        {
            StopAllCoroutines();
            GameController.Instance.NextLevel(true);
        }
    }
    public void stopLevel(int index)
    {
        GameObject level = levels[index];
        foreach (Transform child in level.transform)
        {
            if (child.CompareTag("PeopleFactory"))
            {
                PeopleFactory factory = child.gameObject.GetComponent<PeopleFactory>();
                factory.stopLevel();
            }
            if (child.CompareTag("CorpseGrinderIntersection"))
            {
                AudioSource source = child.gameObject.GetComponent<AudioSource>();
                if (source != null)
                {
                    source.Stop();

                }
            }

        }

    }
    public void startLevelSounds(int index)
    {
        GameObject nextLevel = levels[index];

        foreach (Transform child in nextLevel.transform)
        {
            if (child.CompareTag("CorpseGrinderIntersection"))
            {
                AudioSource source = child.gameObject.GetComponent<AudioSource>();
                if (source != null)
                {
                    source.volume = 0;
                    source.Play();

                }
            }
        }
    }
    public void setSoundVolume(int index,float volume)
    {
        GameObject nextLevel = levels[index];

        foreach (Transform child in nextLevel.transform)
        {
            if (child.CompareTag("CorpseGrinderIntersection"))
            {
                AudioSource source = child.gameObject.GetComponent<AudioSource>();
                if (source != null)
                {
                    source.volume = volume;

                }
            }
        }
    }
    public void startLevel(int index, bool skipAnimation = false)
    {
        GameObject nextLevel = levels[index];

        foreach (Transform child in nextLevel.transform)
        {
            if (child.CompareTag("PeopleFactory"))
            {
                PeopleFactory factory = child.gameObject.GetComponent<PeopleFactory>();
                factory.startLevel();
            }
            if (child.CompareTag("LevelCamera"))
            {
                MoveCameraToPoint(child.gameObject,skipAnimation);
            }
           

        }
        startLevelSounds(index);
    }
    public void removeCorpseGrindersFromLevel(int index)
    {
        GameObject nextLevel = levels[index];

        foreach (Transform child in nextLevel.transform)
        {
            if (child.CompareTag("PeopleFactory"))
            {
                PeopleFactory factory = child.gameObject.GetComponent<PeopleFactory>();
                var humans = factory.GetComponentsInChildren<Human>();
                foreach(Human human in humans)
                {
                    human.movingTowards = false;
                    human.animator.speed = 0f;
                }
            }
                if (child.CompareTag("CorpseGrinderIntersection"))
            {
                CorpseGrinderIntersection intersection = child.gameObject.GetComponent<CorpseGrinderIntersection>();
                intersection.dontKill = true;
            }

        }
    }
    public void moveCameraToNextLevel(bool skipAnimation = false)
    {
        stopLevel(startIndex++);
        startLevel(startIndex, skipAnimation);
        if (startIndex == levels.Count - 1)
        {
            removeCorpseGrindersFromLevel(startIndex - 1);

            GameObject nextLevel = levels[startIndex];
            foreach (Transform child in nextLevel.transform)
            {
                if (child.CompareTag("CorpseGrinderIntersection"))
                {
                    CorpseGrinderIntersection intersection = child.gameObject.GetComponent<CorpseGrinderIntersection>();
                    intersection.GetComponentInChildren<FinalTank>().Play();
                }
            }
        }
    }

    public void moveCameraToPrevLevel(bool skipAnimation = false)
    {
        stopLevel(startIndex--);
        startLevel(startIndex, skipAnimation);
    }

    private void MoveCameraToPoint(GameObject targetPoint, bool skipAnimation = false)
    {
        if (!skipAnimation)
        {
            StartCoroutine(cameraAnimationRoutine(targetPoint));
        }
        else
        {
            gameObject.transform.position = targetPoint.transform.position;
            gameObject.transform.rotation = targetPoint.transform.rotation;
        }
    }

    private IEnumerator cameraAnimationRoutine(GameObject targetPoint)
    {
        Vector3 startPos = gameObject.transform.position;
        Quaternion startRot = gameObject.transform.rotation;
        float elapsedTime = 0f;
        float progress = 0f;
        bool soundStarted = false;

        while (elapsedTime < animTime)
        {
            float currentTime = Mathf.Clamp01(elapsedTime / animTime);
            float translationProgress = animPosCurve.Evaluate(currentTime);
            float rotationProgress = animRotationCurve.Evaluate(currentTime);
            
            Debug.DrawLine(gameObject.transform.position, startPos, Color.red, 0.01f);
            Debug.DrawLine(gameObject.transform.position, targetPoint.transform.position, Color.green, 0.01f);
            gameObject.transform.position = Vector3.Lerp(startPos, targetPoint.transform.position, translationProgress);
            gameObject.transform.rotation = Quaternion.Slerp(startRot, targetPoint.transform.rotation, rotationProgress);
         
            setSoundVolume(startIndex,rotationProgress / 4.0f);
            elapsedTime += Time.deltaTime;
           
            

            yield return null;
        }
    }


    void OnDestroy()
    {
        StopAllCoroutines();
    }
}
