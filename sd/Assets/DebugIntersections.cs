﻿using UnityEngine;
using System.Collections;

public class DebugIntersections : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Intersection[] intersections = GetComponentsInChildren<Intersection>();
        foreach(Intersection intersection in intersections)
        {
            intersection.debugDrawLines();
        }
	}
}
