﻿using UnityEngine;
using System.Collections;

public class FinalTank : MonoBehaviour {

    public AnimationCurve curve;
    public Vector3 startPos = Vector3.zero;
    public float movementDistance = 15f;
    public float currentTime = 0f;
    public float maxTime = 5f;
    public bool isPlaying = false;

	// Use this for initialization
	void Start () {
        startPos = this.gameObject.transform.position;
	}

    public void Play()
    {
        isPlaying = true;
        currentTime = 0f;
    }

	// Update is called once per frame
	void Update () {
	    if (isPlaying)
        {
            currentTime += Time.deltaTime;
           
            if (currentTime <= maxTime)
            {
                this.gameObject.transform.position = startPos +
               this.gameObject.transform.forward * curve.Evaluate(currentTime / maxTime) * movementDistance;
            }
            else
            {
                isPlaying = false;
                GameController.Instance.GameOver(true);
            }
        }   
	}
}
