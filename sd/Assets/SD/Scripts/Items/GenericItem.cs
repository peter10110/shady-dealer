﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GenericItem : MonoBehaviour
{
    private Vector3 spawnPosition;
    public GameObject model;
    public string key;
    public Human carrier;
    public Collider collider;
    public List<Human> possibleCarriers;
    public bool firstHumanAutoPickup = true;
    public AudioSource pickupSound;
    public AudioSource passSound;
    public AudioSource dropSound;

    void Awake()
    {
        spawnPosition = gameObject.transform.position;
    }

    public void ResetItem()
    {
        this.gameObject.transform.parent = null;
        gameObject.transform.position = spawnPosition;
        carrier = null;
        possibleCarriers.Clear();
        firstHumanAutoPickup = true;
    }

    // Use this for initialization
    public void Start()
    {
        GameController.Instance.RegisterItem(this);
        collider = this.GetComponent<Collider>();
    }

    public void OnDestroy()
    {
        if (GameController.Instance != null)
        {
            GameController.Instance.UnRegisterItem(this);
        }
    }

    public void Update()
    {
        if (Input.GetButtonDown(key))
        {
            if (carrier)
            {
                Drop();
            }
            else
            {
                PickUp(GetClosestPossibleCarrier());
            }
        }
    }

    public bool HasCarrier()
    {
        return carrier != null;
    }

    public void PickUp(Human nextCarrier)
    {
        if (nextCarrier == null)
        {
            return;
        }

        carrier = nextCarrier;
        carrier.OnPickupChange(true);
        this.gameObject.transform.SetParent(carrier.transform);
        this.gameObject.transform.localPosition = Vector3.zero;
        model.SetActive(false);
        model.gameObject.transform.localPosition = Vector3.up;
        model.gameObject.transform.localEulerAngles = new Vector3(Random.Range(10f, 170f), Random.Range(10f, 170f), Random.Range(10f, 170f));

        pickupSound.volume = 0.2f;
        pickupSound.Play();

        GameController.Instance.ItemPickedUp(this);
    }

    public void Drop()
    {
        if (carrier == null)
        {
            return;
        }

        GameController.Instance.ItemDropped();

        Human nextPossibleCarrier = GetClosestPossibleCarrier();

        this.gameObject.transform.parent = null;
        carrier.OnPickupChange(false);
        carrier = null;
        model.SetActive(true);


        if (nextPossibleCarrier != null)
        {
            passSound.volume = 0.2f;
            passSound.Play();
            PickUp(nextPossibleCarrier);
        }
        else
        {
            dropSound.volume = 0.15f;
            dropSound.Play();
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Human"))
        {
            Human target = other.gameObject.GetComponent<Human>();
            possibleCarriers.Add(target);
            if (firstHumanAutoPickup)
            {
                PickUp(target);
                firstHumanAutoPickup = false;
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Human"))
        {
            Human target = other.gameObject.GetComponent<Human>();
            possibleCarriers.Remove(target);
        }
    }


    private Human GetClosestPossibleCarrier()
    {
        Human closestHuman = null;
        foreach (var human in possibleCarriers)
        {
            if (carrier == human)
            {
                continue;
            }

            if (carrier != null && carrier.targetIntersection == human.targetIntersection)
            {
                continue;
            }

            float currentDistance = Vector3.Distance(this.gameObject.transform.position, human.gameObject.transform.position);
            if (closestHuman == null)
            {
                closestHuman = human;
            }
            else if (currentDistance < Vector3.Distance(this.gameObject.transform.position, closestHuman.gameObject.transform.position))
            {
                closestHuman = human;
            }
        }

        return closestHuman;
    }
}
