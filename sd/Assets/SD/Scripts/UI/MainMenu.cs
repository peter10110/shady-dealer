﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject buttons;
    public GameObject howToPlay;
    public string gameScene;
    
    public void Start()
    {
        howToPlay.SetActive(false);
        buttons.SetActive(true);
    } 

    public void NewGame()
    {
        SceneManager.LoadScene(gameScene);
    }

    public void ShowHowToPlay()
    {
        buttons.SetActive(false);
        howToPlay.SetActive(true);
    }

    public void HideHowToPlay()
    {
        howToPlay.SetActive(false);
        buttons.SetActive(true);
    }

    public void Exit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
