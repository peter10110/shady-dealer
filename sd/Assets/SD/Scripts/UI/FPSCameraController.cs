﻿using UnityEngine;
using System.Collections;
using System;

public class FPSCameraController : MonoBehaviour
{
    public Camera fpsCamera;
    public GenericItem targetItem;
    public Vector3 cameraOffset = Vector3.zero;
    public GameObject cameraImage;


    // Use this for initialization
    void Start ()
    {
        GameController.OnItemPickUp += OnItemPickUp;
        GameController.OnItemDrop += OnItemDrop;
    }

    private void OnItemPickUp(GenericItem pickedUpItem)
    {
        targetItem = pickedUpItem;
        if (cameraImage.active == false)
        {
            cameraImage.SetActive(true);
        }
    }

    private void OnItemDrop()
    {
        targetItem = null;
    }

    private void LateUpdate()
    {
        if (targetItem != null)
        {
            fpsCamera.transform.position = targetItem.carrier.modelRoot.transform.position + Vector3.up;
            fpsCamera.transform.eulerAngles = targetItem.carrier.transform.eulerAngles;
        }
    }

    private void OnDestroy()
    {
        GameController.OnItemPickUp -= OnItemPickUp;
        GameController.OnItemDrop -= OnItemDrop;
    }
}
