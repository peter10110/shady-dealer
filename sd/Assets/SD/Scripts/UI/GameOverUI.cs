﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour {

    public Text GameOverText;
    public string winText = "You've managed to deliver the weapon!";
    public string loseText = "Game Over";
    public GameObject GameOverUIParent;

	void Start ()
    {
        GameController.OnGameOver += ShowGUI;
    }

    private void OnDestroy()
    {
        GameController.OnGameOver -= ShowGUI;
    }

    private void ShowGUI(bool won)
    {
        GameOverText.text = won ? winText : loseText;
        GameOverUIParent.SetActive(true);
    }
}
