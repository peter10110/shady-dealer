﻿using UnityEngine;
using System.Collections;

public class LineOfSightHandler : MonoBehaviour
{

    public AVO self;
    public Rigidbody rBody;

    private void Awake()
    {
        rBody = this.GetComponent<Rigidbody>();
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Item"))
        {
            if (!other.gameObject.GetComponent<GenericItem>().HasCarrier())
            {
                GameController.Instance.GameOver(false);
            }
        }
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
