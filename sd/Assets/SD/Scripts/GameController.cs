﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameController : MonoBehaviour {

    public static GameController Instance;
    public List<GenericItem> items;
    public delegate void GameOverDelegate(bool won);
    public static GameOverDelegate OnGameOver;
    public delegate void ItemPickUpDelegate(GenericItem pickedUpItem);
    public static ItemPickUpDelegate OnItemPickUp;
    public delegate void ItemDroppedDelegate();
    public static ItemDroppedDelegate OnItemDrop;
    public CameraController camera;
    public static int currentlevelNum = 0;
    public static int targetlevelNum = 0;

    public void UnRegisterItem(GenericItem genericItem)
    {
        items.Remove(genericItem);
    }

    private void Awake()
    {
        Instance = this;
        items = new List<GenericItem>();
    }

    public void RegisterItem(GenericItem item)
    {
        items.Add(item);
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public bool HasItem(Human human)
    {
        foreach (var item in items)
        {
            if (item.carrier == human)
            {
                return true;
            }
        }
        return false;
    }

    public void NextLevel(bool skipAnimation = false)
    {
        // Reset all items
        foreach (var item in items)
        {
            item.ResetItem();
        }

        currentlevelNum++;
        camera.moveCameraToNextLevel(skipAnimation);
    }

    public void PrevLevel(bool skipAnimation = false)
    {
        // Reset all items
        foreach (var item in items)
        {
            item.ResetItem();
        }

        currentlevelNum--;
        camera.moveCameraToPrevLevel(skipAnimation);
    }

    public void GameOver(bool won)
    {
        if (OnGameOver != null)
        {
            OnGameOver(won);
        }
    }

    public void BackToMenu()
    {
        // Ez jelenleg quit.
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void Retry()
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        targetlevelNum = currentlevelNum;
        currentlevelNum = 0;
        SceneManager.LoadScene(currentSceneName);
    }

    public void ItemPickedUp(GenericItem genericItem)
    {
        if (OnItemPickUp != null)
        {
            OnItemPickUp(genericItem);
        }
    }

    internal void ItemDropped()
    {
        if (OnItemDrop != null)
        {
            OnItemDrop();
        }
    }

    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetButtonDown("Fire3"))
        {
            if (UnityEngine.Time.timeScale == 5)
            {
                UnityEngine.Time.timeScale = 1;
            }
            else
            {
                UnityEngine.Time.timeScale = 5;
            }
        }
#endif
    }
}
