﻿using UnityEngine;
using System.Collections;

public class searching_avo : MonoBehaviour
{
    public Intersection[] grinders;
    public StopperIntersection stopIntersection;

    public int searchInterval = 5;
    public int killInterval = 3;

    public int killCounter = 0;


    public int searchCounter = 0;

    public float searchTime = 2f;

    public bool searching = false;

    Human stoppedSpeciemen;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private Intersection human_originalTarget;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Human") && !searching)
        {
            searchCounter++;
            if (searchCounter >= searchInterval)
            {
                searching = true;
                stoppedSpeciemen = other.GetComponent<Human>();
                searchCounter = 0;
                human_originalTarget = stoppedSpeciemen.targetIntersection;
                stoppedSpeciemen.GoToIntersection(stopIntersection);
            }
        }
    }

    public void SpeciemenArrived()
    {
        if (GameController.Instance.HasItem(stoppedSpeciemen))
        {
            Debug.Log("Itt erősen véget kéne, hogy érjen a játék");
            GameController.Instance.GameOver(false);
        }
        else
        {
            StartCoroutine(SearchTimer());
        }
    }

    private IEnumerator SearchTimer()
    {
        float timer = 0f;
        while (timer <= searchTime)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        killCounter++;
        if (killCounter >= killInterval && grinders.Length > 0)
        {
            killCounter = 0;
            stoppedSpeciemen.GoToIntersection(grinders[Random.Range(0, grinders.Length)]);
        }
        else
        {
            stoppedSpeciemen.GoToIntersection(human_originalTarget);
        }
        stoppedSpeciemen = null;
        searching = false;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
