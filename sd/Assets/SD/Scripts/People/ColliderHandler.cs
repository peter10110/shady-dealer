﻿using UnityEngine;
using System.Collections;

public class ColliderHandler : MonoBehaviour
{
    public Human self;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Intersection"))
        {
            Intersection target = other.gameObject.GetComponent<Intersection>();
            if (target != null)
            {
                self.SetTargetIntersection(target);
            }
            else
            {
                Debug.Log("Ohh bazmeg, ez nincs itt.....");
            }
        }
        else
        {
            Debug.Log("Not intersection!!!!");
        }
    }
}
