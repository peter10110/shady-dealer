﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Human : MonoBehaviour {

    public float movementSpeed = 1f;
    public Intersection targetIntersection;
    public AnimationCurve movementDistortion;
    public bool movingTowards = false;
    public float movementDistance = 0f;
    public float currentDistance = 0f;
    public Vector3 movementDirection = Vector3.zero;
    public Rigidbody rBody;
    public GameObject modelRoot;
    public List<GameObject> models;
    public Vector3 modelStartOffset;
    public float rotationStepSize = 1f;
    public float offsetMultiplier = 3f;
    public List<Material> meshMaterials;
    private int shaderOutlineColorID;
    public GameObject parentFactory;
    public float[] validStartTimes = new float[4];
    public Color baseColor;
    public Animator animator;
    private bool isAnimationPlaying = true;

    private float getValidStartTime()
    {
        return validStartTimes[Random.Range(0, validStartTimes.Length)] * movementDistance;
    }

    public void setFirstTarget(Intersection target)
    {
        targetIntersection = target;
        movingTowards = true;
        movementDirection = (targetIntersection.Position - this.gameObject.transform.position).normalized;
        movementDistance = (targetIntersection.Position - this.gameObject.transform.position).magnitude;
        currentDistance = getValidStartTime();
    }


    public void SetTargetIntersection(SortBySourceIntersection target)
    {
        if (target == null)
        {
            return;
        }
        targetIntersection = target.GetNextDestinationByHumanSource(this);
        if (targetIntersection == null)
        {
            return;
        }
        movingTowards = true;
        movementDirection = (targetIntersection.Position - this.gameObject.transform.position).normalized;
        movementDistance = (targetIntersection.Position - this.gameObject.transform.position).magnitude;
        currentDistance = getValidStartTime();
        //StopAllCoroutines();
        //StartCoroutine(RotateToDirection(movementDirection));
    }
    public void SetTargetIntersection(Intersection target)
    {
        if (target == null)
        {
            movingTowards = false;
            return;
        }
        targetIntersection = target.GetNextDestination(movementDirection);
        if (targetIntersection == null)
        {
            movingTowards = false;
            return;
        }
        movingTowards = true;
        movementDirection = (targetIntersection.Position - this.gameObject.transform.position).normalized;
        movementDistance = (targetIntersection.Position - this.gameObject.transform.position).magnitude;
        currentDistance = getValidStartTime();
    }

    public void GoToIntersection(Intersection targetInt)
    {
        movingTowards = true;
        targetIntersection = targetInt;
        movementDirection = (targetIntersection.Position - this.gameObject.transform.position).normalized;
        movementDistance = (targetIntersection.Position - this.gameObject.transform.position).magnitude;
        currentDistance = getValidStartTime();
    }

    private void Awake()
    {
        foreach (var model in models)
        {
            meshMaterials.Add(model.GetComponent<SkinnedMeshRenderer>().material);
        }
        shaderOutlineColorID = Shader.PropertyToID("_OutlineColor");
    }

    // Use this for initialization
    void Start() {
        GameController.OnGameOver += OnGameOver;
        modelStartOffset = modelRoot.transform.localPosition;
    }

    void OnDestroy()
    {
        GameController.OnGameOver -= OnGameOver;
    }

    public void OnGameOver(bool won)
    {
        movingTowards = false;
    }

    public void OnPickupChange(bool pickedUp)
    {
        foreach (var material in meshMaterials)
        {
            material.SetColor(shaderOutlineColorID, pickedUp ? Color.green : baseColor );
        }
    }

    // Update is called once per frame
    void Update() {
        if (movingTowards)
        {
            if (!isAnimationPlaying)
            {
                isAnimationPlaying = true;
                animator.speed = 1f;
            }

            if (Vector3.Angle(this.transform.forward, movementDirection) > 1f)
            {
                Vector3 rotation = Vector3.RotateTowards(this.transform.forward, movementDirection, rotationStepSize * Time.deltaTime, float.PositiveInfinity);
                this.transform.rotation = Quaternion.LookRotation(rotation);
            }

            float offset = movementDistortion.Evaluate(currentDistance / movementDistance) * offsetMultiplier * movementDistance;
            Vector3 distortedDirection = Vector3.Cross(movementDirection, Vector3.up);
            modelRoot.transform.localPosition = modelStartOffset + new Vector3(offset, 0f, 0f);
        }
        else if (isAnimationPlaying)
        {
            isAnimationPlaying = false;
            animator.speed = 0f;
        }
    }

    private void FixedUpdate()
    {
        if (movingTowards)
        {
            Vector3 movementDelta = movementDirection * Time.fixedDeltaTime * movementSpeed;
            rBody.MovePosition(rBody.position + movementDelta);
            currentDistance += movementDelta.magnitude;
            if (currentDistance > movementDistance)
            {
                currentDistance = 0f;
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Intersection") ||
            other.gameObject.CompareTag("NextLevelIntersection"))

        {
            Intersection target = other.gameObject.GetComponent<Intersection>();
            if (target != null)
            {
                SetTargetIntersection(target);
            }
            else
            {
                Debug.Log("Ohh bazmeg, ez nincs itt.....");
            }
        } else if (other.gameObject.CompareTag("CorpseGrinderIntersection")){
            CorpseGrinderIntersection target = other.gameObject.GetComponent<CorpseGrinderIntersection>();
            target.justKillMeAlready(this);

        } else if (other.gameObject.CompareTag("SortBySourceIntersection"))
        {
            SortBySourceIntersection target = other.gameObject.GetComponent<SortBySourceIntersection>();
            if (target != null)
            {
                SetTargetIntersection(target);
            }
        }
        if (other.gameObject.CompareTag("NextLevelIntersection"))
        {
            NextLevelIntersection target = other.gameObject.GetComponent<NextLevelIntersection>();
            target.decideGameEnding(this);

        }
        
    }
}
