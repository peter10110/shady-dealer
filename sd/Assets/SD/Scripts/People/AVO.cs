﻿using UnityEngine;
using System.Collections;

public class AVO : MonoBehaviour
{
    public GameObject body;
    public GameObject lineOfSightObject;
    public Rigidbody lineOfSightBody;
    public float rotationTime = 4f;
    public float maxRotation = 45f;
    public float currentRotation = 0f;
    public float elapsedRotationTime = 0f;
    public AnimationCurve curve;

    // Update is called once per frame
    void Update()
    {
        if (elapsedRotationTime <= rotationTime)
        {
            elapsedRotationTime += Time.deltaTime;
            float curveValue = curve.Evaluate(elapsedRotationTime/rotationTime);
            currentRotation = curveValue * maxRotation;
        }
        else
        {
            elapsedRotationTime = 0f;
        }
    }

    public void FixedUpdate()
    {
        lineOfSightObject.transform.localEulerAngles = new Vector3(0f, currentRotation - (maxRotation / 2.0f), 0f);
    }

    private void MoveSelf()
    {

    }
}
