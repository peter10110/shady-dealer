﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BackgroundMusic : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.DontDestroyOnLoad(this.gameObject);
        SceneManager.LoadScene(1);
	}
}
