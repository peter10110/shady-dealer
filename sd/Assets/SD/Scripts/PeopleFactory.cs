﻿using UnityEngine;
using System.Collections;

public class PeopleFactory : MonoBehaviour {
    public float timeBetweenPeopleSpawn;
    public float spawnStartTime;
    public Intersection targetIntersection;
    public Human human;
    private int spawnedPeopleNum = 0;
    public int maxSpawnedPeople = 0;
    public Transform peopleParent;
    public bool InfiniteSpawn = false;

	// Use this for initialization
	void Start () {
        GameController.OnGameOver += OnGameOver;
	}

    void OnDestroy()
    {
        GameController.OnGameOver -= OnGameOver;
    }

    private void OnGameOver(bool won)
    {
        CancelInvoke("spawnPeople");
    }

    public void startLevel()
    {
        // Clean up people
        foreach (Transform child in transform)
        {
            if (child.CompareTag("Human"))
            {
                Destroy(child.gameObject);
            }
        }

        // Start spawning
        InvokeRepeating("spawnPeople", spawnStartTime, timeBetweenPeopleSpawn);
    }
    public void stopLevel()
    {
        // Stop spawning
        CancelInvoke("spawnPeople");

        // Clean up people
        foreach (Transform child in transform)
        {
            if (child.CompareTag("Human"))
            {
                Destroy(child.gameObject);
            }
        }
    }
    void spawnPeople()
    {
        Human people = (Human)Instantiate(human,transform.position,Quaternion.identity,transform);
	    people.setFirstTarget(targetIntersection);
        people.gameObject.name = "Human" + spawnedPeopleNum;
        people.parentFactory = this.gameObject;
        spawnedPeopleNum++;
        if (spawnedPeopleNum > maxSpawnedPeople && !InfiniteSpawn)
        {
            CancelInvoke("spawnPeople");
        }
    }
}
