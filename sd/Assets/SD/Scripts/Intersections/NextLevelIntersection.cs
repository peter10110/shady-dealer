﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



class NextLevelIntersection : Intersection 
{
    public CameraController camera;

    public Mauser mauser;
    public override Intersection GetNextDestination(Vector3 humanMovementDir)
    {
        return base.GetNextDestination(humanMovementDir);
    }
    public void decideGameEnding(Human human)
    {
        if(mauser.carrier == human)
        {
            changeScene();
            Object.Destroy(mauser.gameObject);
        }
    }



    public void changeScene()
    {
        camera.moveCameraToNextLevel();
    }
}

