﻿using UnityEngine;

public class SortBySourceIntersection : Intersection
 {

    public PeopleFactory[] toUp = new PeopleFactory[1];
    public PeopleFactory[] toDown = new PeopleFactory[1];
    public PeopleFactory[] toLeft = new PeopleFactory[1];
    public PeopleFactory[] toRight = new PeopleFactory[1];

    public Intersection UP;
    public Intersection DOWN;
    public Intersection LEFT;
    public Intersection RIGHT;


    protected override void drawLinesToIntersections(Intersection[] inters, Directions direction)
    {
        if(UP != null)
        {
            Debug.DrawLine((this.Position) + new Vector3(0, 1, 0), UP.Position, Color.magenta);
        }
        if (DOWN != null)
        {
            Debug.DrawLine((this.Position) + new Vector3(0, 1, 0), DOWN.Position, Color.magenta);
        }
        if (LEFT != null)
        {
            Debug.DrawLine((this.Position) + new Vector3(0, 1, 0), LEFT.Position, Color.magenta);
        }
        if (RIGHT != null)
        {
            Debug.DrawLine((this.Position) + new Vector3(0, 1, 0), RIGHT.Position, Color.magenta);
        }

    }


    public override Intersection GetNextDestination(Vector3 humanMovementDir)
    {
        return null;
    }

    public Intersection GetNextDestinationByHumanSource(Human human)
    {
        foreach(PeopleFactory pf in toUp){
            if(pf == null)
            {
                continue;
            }
            if(human.parentFactory == pf.gameObject)
            {
                return UP;
            }
        }
        foreach (PeopleFactory pf in toDown)
        {
            if (pf == null)
            {
                continue;
            }
            if (human.parentFactory == pf.gameObject)
            {
                return DOWN;
            }
        }
        foreach (PeopleFactory pf in toRight)
        {
            if (pf == null)
            {
                continue;
            }
            if (human.parentFactory == pf.gameObject)
            {
                return RIGHT;
            }
        }
        foreach (PeopleFactory pf in toLeft)
        {
            if (pf == null)
            {
                continue;
            }
            if (human.parentFactory == pf.gameObject)
            {
                return LEFT;
            }
        }

        return null;

    }

 }
