﻿using UnityEngine;
using System.Collections;

public class CorpseGrinderIntersection : Intersection {
    public ParticleSystem bloodParticle;
    public bool dontKill = false;
    public void justKillMeAlready(Human human)
    {
        if (dontKill)
        {
            return;
        }
        if (bloodParticle != null)
        {
            bloodParticle.Play();
        }
        bool gameOver = GameController.Instance.HasItem(human);
        Object.Destroy(human.gameObject);
        if (gameOver)
        {
            GameController.Instance.GameOver(false);
        }
    }

}
