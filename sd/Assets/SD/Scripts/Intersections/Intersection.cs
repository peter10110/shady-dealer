﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Directions
{
    Up = 0,
    Down = 1,
    Left = 2,
    Right = 3
}

public class Intersection : MonoBehaviour {

    public BoxCollider collider;
    public Intersection[] z_fromUp = new Intersection[1];
    public Intersection[] z_fromDown= new Intersection[1];
    public Intersection[] x_fromLeft= new Intersection[1];
    public Intersection[] x_fromRight= new Intersection[1];

    private  int[]  targetStatus = new int[4] { -1,-1, -1, -1};

    private Color getColorFromDirection(Directions direction)
    {
        switch (direction)
        {
            case Directions.Up:
                return Color.yellow;
                break;
            case Directions.Down:
                return Color.green;
                break;
            case Directions.Left:
                return Color.red;
                break;
            case Directions.Right:
                return Color.blue;
                break;
            default:
                return Color.yellow;
                break;
        }
    }
    protected virtual void drawLinesToIntersections(Intersection[] inters,Directions direction)
    {
        foreach (Intersection intersection in inters)
        {
            if (intersection != null)
            {
                Debug.DrawLine((this.Position)+new Vector3(0,1,0), intersection.Position,getColorFromDirection(direction));
            }
        }
    }
    public void debugDrawLines()
    {
        drawLinesToIntersections(z_fromUp,Directions.Up);
        drawLinesToIntersections(z_fromDown,Directions.Down);
        drawLinesToIntersections(x_fromLeft,Directions.Left);
        drawLinesToIntersections(x_fromRight,Directions.Right);
    }

    public Vector3 Position {
        get
        {
            return this.transform.position;
        }
    }

    
    private Intersection getNextIntersection(Intersection[] intersections,int targetStatusIndex)
    {
        int size = intersections.Length;
        targetStatus[targetStatusIndex] = (targetStatus[targetStatusIndex] + 1 ) % size ;

        return intersections[targetStatus[targetStatusIndex]];

    }
    public virtual Intersection GetNextDestination(Vector3 humanMovementDir)
    {
        Directions targetDir = GetFromDirection(humanMovementDir * -1f);
        switch (targetDir)
        {
            case Directions.Up:
                return getNextIntersection(z_fromUp,(int)Directions.Up);
            case Directions.Down:
                return getNextIntersection(z_fromDown,(int)Directions.Down);
            case Directions.Left:
                return getNextIntersection(x_fromLeft,(int)Directions.Left);
            case Directions.Right:
                return getNextIntersection(x_fromRight,(int)Directions.Right);
            default:
                return null;
        }
    }

    //private Vector3 GetTarget(Directions dir)
    //{
    //    Vector3 retVal = (m_intersections[(int)dir].transform.position - transform.position).normalized;
    //    return retVal;
    //}

    private static float TheValue = 0.70710678118654752440084436210485f;
    private Directions GetFromDirection(Vector3 inromingDir)
    {
        if(Vector3.Dot(-inromingDir, Vector3.back) > TheValue)
        {
            return Directions.Up;
        }
        else if (Vector3.Dot(-inromingDir, Vector3.forward) > TheValue)
        {
            return Directions.Down;
        }
        else if (Vector3.Dot(-inromingDir, Vector3.left) > TheValue)
        {
            return Directions.Right;
        }
        else if (Vector3.Dot(-inromingDir, Vector3.right) > TheValue)
        {
            return Directions.Left;
        }
        return Directions.Up;
    }
}
