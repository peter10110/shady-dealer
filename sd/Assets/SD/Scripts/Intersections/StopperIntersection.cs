﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class StopperIntersection : Intersection
{
    public searching_avo avo;

    public override Intersection GetNextDestination(Vector3 humanMovementDir)
    {
        avo.SpeciemenArrived();
        return null;
    }

}

