// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.10 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.10;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,nrsp:0,limd:3,spmd:1,grmd:1,uamb:True,mssp:True,bkdf:True,rprd:True,enco:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:1,culm:0,dpts:2,wrdp:True,dith:0,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:6565,x:33871,y:32399,varname:node_6565,prsc:2|diff-2674-OUT,spec-3230-R,gloss-8459-OUT,normal-4082-RGB;n:type:ShaderForge.SFN_Tex2d,id:840,x:32515,y:32338,ptovrint:False,ptlb:Albedo,ptin:_Albedo,varname:node_840,prsc:2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:3230,x:32412,y:32723,ptovrint:False,ptlb:Metal(R)Rough(G)Emissive(B)AO(A),ptin:_MetalRRoughGEmissiveBAOA,varname:node_3230,prsc:2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4082,x:32550,y:32939,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_4082,prsc:2,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:8459,x:33376,y:32705,varname:node_8459,prsc:2|A-7077-OUT,B-3230-G;n:type:ShaderForge.SFN_Slider,id:7077,x:32962,y:32687,ptovrint:False,ptlb:Roughness_STR,ptin:_Roughness_STR,varname:node_7077,prsc:2,min:0,cur:0,max:5;n:type:ShaderForge.SFN_Color,id:8290,x:32648,y:32180,ptovrint:False,ptlb:DiffColor,ptin:_DiffColor,varname:node_8290,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:7897,x:32916,y:32280,varname:node_7897,prsc:2|A-8290-RGB,B-840-RGB;n:type:ShaderForge.SFN_Lerp,id:2674,x:33470,y:31817,varname:node_2674,prsc:2|A-4117-OUT,B-7897-OUT,T-6731-OUT;n:type:ShaderForge.SFN_Tex2d,id:6776,x:33226,y:31506,ptovrint:False,ptlb:Sand,ptin:_Sand,varname:node_6776,prsc:2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_NormalVector,id:4294,x:31965,y:31706,prsc:2,pt:True;n:type:ShaderForge.SFN_ComponentMask,id:9784,x:32184,y:31681,varname:node_9784,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4294-OUT;n:type:ShaderForge.SFN_Subtract,id:2751,x:32635,y:31696,varname:node_2751,prsc:2|A-5474-OUT,B-5482-OUT;n:type:ShaderForge.SFN_Slider,id:5482,x:32045,y:31921,ptovrint:False,ptlb:Slope,ptin:_Slope,varname:node_5482,prsc:2,min:-1,cur:0.489816,max:1;n:type:ShaderForge.SFN_OneMinus,id:5474,x:32375,y:31691,varname:node_5474,prsc:2|IN-9784-OUT;n:type:ShaderForge.SFN_Multiply,id:6774,x:32816,y:31781,varname:node_6774,prsc:2|A-2751-OUT,B-1593-OUT;n:type:ShaderForge.SFN_Slider,id:1593,x:32453,y:31955,ptovrint:False,ptlb:Hardness,ptin:_Hardness,varname:node_1593,prsc:2,min:0,cur:0,max:10;n:type:ShaderForge.SFN_Clamp01,id:6731,x:33029,y:31761,varname:node_6731,prsc:2|IN-6774-OUT;n:type:ShaderForge.SFN_Color,id:2342,x:33180,y:31321,ptovrint:False,ptlb:SandColor,ptin:_SandColor,varname:node_2342,prsc:2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:4117,x:33531,y:31546,varname:node_4117,prsc:2|A-2342-RGB,B-6776-RGB;n:type:ShaderForge.SFN_Slider,id:488,x:33121,y:32398,ptovrint:False,ptlb:SandRough,ptin:_SandRough,varname:node_488,prsc:2,min:0,cur:0.75,max:1;n:type:ShaderForge.SFN_Multiply,id:2310,x:33383,y:32162,varname:node_2310,prsc:2|A-6776-A,B-488-OUT;n:type:ShaderForge.SFN_Lerp,id:4623,x:33585,y:32171,varname:node_4623,prsc:2|A-2310-OUT,B-8459-OUT,T-6731-OUT;proporder:8290-840-4082-3230-7077-2342-6776-5482-1593-488;pass:END;sub:END;*/

Shader "Shader Forge/NemesysRock" {
    Properties {
        _DiffColor ("DiffColor", Color) = (1,1,1,1)
        _Albedo ("Albedo", 2D) = "white" {}
        _Normal ("Normal", 2D) = "bump" {}
        _MetalRRoughGEmissiveBAOA ("Metal(R)Rough(G)Emissive(B)AO(A)", 2D) = "white" {}
        _Roughness_STR ("Roughness_STR", Range(0, 5)) = 0
        _SandColor ("SandColor", Color) = (1,1,1,1)
        _Sand ("Sand", 2D) = "white" {}
        _Slope ("Slope", Range(-1, 1)) = 0.489816
        _Hardness ("Hardness", Range(0, 10)) = 0
        _SandRough ("SandRough", Range(0, 1)) = 0.75
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers opengl gles gles3 metal d3d11_9x xbox360 ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform sampler2D _MetalRRoughGEmissiveBAOA; uniform float4 _MetalRRoughGEmissiveBAOA_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _Roughness_STR;
            uniform float4 _DiffColor;
            uniform sampler2D _Sand; uniform float4 _Sand_ST;
            uniform float _Slope;
            uniform float _Hardness;
            uniform float4 _SandColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
            #endif
            #ifdef DYNAMICLIGHTMAP_ON
                o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
            #endif
            o.normalDir = UnityObjectToWorldNormal(v.normal);
            o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
            o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            float3 lightColor = _LightColor0.rgb;
            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
            UNITY_TRANSFER_FOG(o,o.pos);
            TRANSFER_VERTEX_TO_FRAGMENT(o)
            return o;
        }
        float4 frag(VertexOutput i) : COLOR {
            i.normalDir = normalize(i.normalDir);
            float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
            float3 normalLocal = _Normal_var.rgb;
            float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
            float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
            float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
            float3 lightColor = _LightColor0.rgb;
            float3 halfDirection = normalize(viewDirection+lightDirection);
// Lighting:
            float attenuation = LIGHT_ATTENUATION(i);
            float3 attenColor = attenuation * _LightColor0.xyz;
            float Pi = 3.141592654;
            float InvPi = 0.31830988618;
///// Gloss:
            float4 _MetalRRoughGEmissiveBAOA_var = tex2D(_MetalRRoughGEmissiveBAOA,TRANSFORM_TEX(i.uv0, _MetalRRoughGEmissiveBAOA));
            float node_8459 = (_Roughness_STR*_MetalRRoughGEmissiveBAOA_var.g);
            float gloss = 1.0 - node_8459; // Convert roughness to gloss
            float specPow = exp2( gloss * 10.0+1.0);
/// GI Data:
            UnityLight light;
            #ifdef LIGHTMAP_OFF
                light.color = lightColor;
                light.dir = lightDirection;
                light.ndotl = LambertTerm (normalDirection, light.dir);
            #else
                light.color = half3(0.f, 0.f, 0.f);
                light.ndotl = 0.0f;
                light.dir = half3(0.f, 0.f, 0.f);
            #endif
            UnityGIInput d;
            d.light = light;
            d.worldPos = i.posWorld.xyz;
            d.worldViewDir = viewDirection;
            d.atten = attenuation;
            #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                d.ambient = 0;
                d.lightmapUV = i.ambientOrLightmapUV;
            #else
                d.ambient = i.ambientOrLightmapUV;
            #endif
            d.boxMax[0] = unity_SpecCube0_BoxMax;
            d.boxMin[0] = unity_SpecCube0_BoxMin;
            d.probePosition[0] = unity_SpecCube0_ProbePosition;
            d.probeHDR[0] = unity_SpecCube0_HDR;
            d.boxMax[1] = unity_SpecCube1_BoxMax;
            d.boxMin[1] = unity_SpecCube1_BoxMin;
            d.probePosition[1] = unity_SpecCube1_ProbePosition;
            d.probeHDR[1] = unity_SpecCube1_HDR;
            UnityGI gi = UnityGlobalIllumination (d, 1, gloss, normalDirection);
            lightDirection = gi.light.dir;
            lightColor = gi.light.color;
// Specular:
            float NdotL = max(0, dot( normalDirection, lightDirection ));
            float LdotH = max(0.0,dot(lightDirection, halfDirection));
            float4 _Sand_var = tex2D(_Sand,TRANSFORM_TEX(i.uv0, _Sand));
            float4 _Albedo_var = tex2D(_Albedo,TRANSFORM_TEX(i.uv0, _Albedo));
            float node_6731 = saturate((((1.0 - normalDirection.g)-_Slope)*_Hardness));
            float3 diffuseColor = lerp((_SandColor.rgb*_Sand_var.rgb),(_DiffColor.rgb*_Albedo_var.rgb),node_6731); // Need this for specular when using metallic
            float specularMonochrome;
            float3 specularColor;
            diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, _MetalRRoughGEmissiveBAOA_var.r, specularColor, specularMonochrome );
            specularMonochrome = 1-specularMonochrome;
            float NdotV = max(0.0,dot( normalDirection, viewDirection ));
            float NdotH = max(0.0,dot( normalDirection, halfDirection ));
            float VdotH = max(0.0,dot( viewDirection, halfDirection ));
            float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
            float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
            float specularPBL = max(0, (NdotL*visTerm*normTerm) * unity_LightGammaCorrectionConsts_PIDiv4 );
            float3 directSpecular = 1 * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
            half grazingTerm = saturate( gloss + specularMonochrome );
            float3 indirectSpecular = (gi.indirect.specular);
            indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
            float3 specular = (directSpecular + indirectSpecular);
/// Diffuse:
            NdotL = max(0.0,dot( normalDirection, lightDirection ));
            half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
            float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
            float3 indirectDiffuse = float3(0,0,0);
            indirectDiffuse += gi.indirect.diffuse;
            float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
// Final Color:
            float3 finalColor = diffuse + specular;
            fixed4 finalRGBA = fixed4(finalColor,1);
            UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
            return finalRGBA;
        }
        ENDCG
    }
    Pass {
        Name "FORWARD_DELTA"
        Tags {
            "LightMode"="ForwardAdd"
        }
        Blend One One
        
        
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #define UNITY_PASS_FORWARDADD
        #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
        #define _GLOSSYENV 1
        #include "UnityCG.cginc"
        #include "AutoLight.cginc"
        #include "Lighting.cginc"
        #include "UnityPBSLighting.cginc"
        #include "UnityStandardBRDF.cginc"
        #pragma multi_compile_fwdadd_fullshadows
        #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
        #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
        #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
        #pragma multi_compile_fog
        #pragma exclude_renderers opengl gles gles3 metal d3d11_9x xbox360 ps3 ps4 psp2 
        #pragma target 3.0
        uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
        uniform sampler2D _MetalRRoughGEmissiveBAOA; uniform float4 _MetalRRoughGEmissiveBAOA_ST;
        uniform sampler2D _Normal; uniform float4 _Normal_ST;
        uniform float _Roughness_STR;
        uniform float4 _DiffColor;
        uniform sampler2D _Sand; uniform float4 _Sand_ST;
        uniform float _Slope;
        uniform float _Hardness;
        uniform float4 _SandColor;
        struct VertexInput {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
            float4 tangent : TANGENT;
            float2 texcoord0 : TEXCOORD0;
            float2 texcoord1 : TEXCOORD1;
            float2 texcoord2 : TEXCOORD2;
        };
        struct VertexOutput {
            float4 pos : SV_POSITION;
            float2 uv0 : TEXCOORD0;
            float2 uv1 : TEXCOORD1;
            float2 uv2 : TEXCOORD2;
            float4 posWorld : TEXCOORD3;
            float3 normalDir : TEXCOORD4;
            float3 tangentDir : TEXCOORD5;
            float3 bitangentDir : TEXCOORD6;
            LIGHTING_COORDS(7,8)
        };
        VertexOutput vert (VertexInput v) {
            VertexOutput o = (VertexOutput)0;
            o.uv0 = v.texcoord0;
            o.uv1 = v.texcoord1;
            o.uv2 = v.texcoord2;
            o.normalDir = UnityObjectToWorldNormal(v.normal);
            o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
            o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            float3 lightColor = _LightColor0.rgb;
            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
            TRANSFER_VERTEX_TO_FRAGMENT(o)
            return o;
        }
        float4 frag(VertexOutput i) : COLOR {
            i.normalDir = normalize(i.normalDir);
            float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
            float3 normalLocal = _Normal_var.rgb;
            float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
            float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
            float3 lightColor = _LightColor0.rgb;
            float3 halfDirection = normalize(viewDirection+lightDirection);
// Lighting:
            float attenuation = LIGHT_ATTENUATION(i);
            float3 attenColor = attenuation * _LightColor0.xyz;
            float Pi = 3.141592654;
            float InvPi = 0.31830988618;
///// Gloss:
            float4 _MetalRRoughGEmissiveBAOA_var = tex2D(_MetalRRoughGEmissiveBAOA,TRANSFORM_TEX(i.uv0, _MetalRRoughGEmissiveBAOA));
            float node_8459 = (_Roughness_STR*_MetalRRoughGEmissiveBAOA_var.g);
            float gloss = 1.0 - node_8459; // Convert roughness to gloss
            float specPow = exp2( gloss * 10.0+1.0);
// Specular:
            float NdotL = max(0, dot( normalDirection, lightDirection ));
            float LdotH = max(0.0,dot(lightDirection, halfDirection));
            float4 _Sand_var = tex2D(_Sand,TRANSFORM_TEX(i.uv0, _Sand));
            float4 _Albedo_var = tex2D(_Albedo,TRANSFORM_TEX(i.uv0, _Albedo));
            float node_6731 = saturate((((1.0 - normalDirection.g)-_Slope)*_Hardness));
            float3 diffuseColor = lerp((_SandColor.rgb*_Sand_var.rgb),(_DiffColor.rgb*_Albedo_var.rgb),node_6731); // Need this for specular when using metallic
            float specularMonochrome;
            float3 specularColor;
            diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, _MetalRRoughGEmissiveBAOA_var.r, specularColor, specularMonochrome );
            specularMonochrome = 1-specularMonochrome;
            float NdotV = max(0.0,dot( normalDirection, viewDirection ));
            float NdotH = max(0.0,dot( normalDirection, halfDirection ));
            float VdotH = max(0.0,dot( viewDirection, halfDirection ));
            float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
            float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
            float specularPBL = max(0, (NdotL*visTerm*normTerm) * unity_LightGammaCorrectionConsts_PIDiv4 );
            float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
            float3 specular = directSpecular;
/// Diffuse:
            NdotL = max(0.0,dot( normalDirection, lightDirection ));
            half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
            float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
            float3 diffuse = directDiffuse * diffuseColor;
// Final Color:
            float3 finalColor = diffuse + specular;
            return fixed4(finalColor * 1,0);
        }
        ENDCG
    }
    Pass {
        Name "Meta"
        Tags {
            "LightMode"="Meta"
        }
        Cull Off
        
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #define UNITY_PASS_META 1
        #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
        #define _GLOSSYENV 1
        #include "UnityCG.cginc"
        #include "Lighting.cginc"
        #include "UnityPBSLighting.cginc"
        #include "UnityStandardBRDF.cginc"
        #include "UnityMetaPass.cginc"
        #pragma fragmentoption ARB_precision_hint_fastest
        #pragma multi_compile_shadowcaster
        #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
        #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
        #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
        #pragma multi_compile_fog
        #pragma exclude_renderers opengl gles gles3 metal d3d11_9x xbox360 ps3 ps4 psp2 
        #pragma target 3.0
        uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
        uniform sampler2D _MetalRRoughGEmissiveBAOA; uniform float4 _MetalRRoughGEmissiveBAOA_ST;
        uniform float _Roughness_STR;
        uniform float4 _DiffColor;
        uniform sampler2D _Sand; uniform float4 _Sand_ST;
        uniform float _Slope;
        uniform float _Hardness;
        uniform float4 _SandColor;
        struct VertexInput {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
            float2 texcoord0 : TEXCOORD0;
            float2 texcoord1 : TEXCOORD1;
            float2 texcoord2 : TEXCOORD2;
        };
        struct VertexOutput {
            float4 pos : SV_POSITION;
            float2 uv0 : TEXCOORD0;
            float2 uv1 : TEXCOORD1;
            float2 uv2 : TEXCOORD2;
            float4 posWorld : TEXCOORD3;
            float3 normalDir : TEXCOORD4;
        };
        VertexOutput vert (VertexInput v) {
            VertexOutput o = (VertexOutput)0;
            o.uv0 = v.texcoord0;
            o.uv1 = v.texcoord1;
            o.uv2 = v.texcoord2;
            o.normalDir = UnityObjectToWorldNormal(v.normal);
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
            return o;
        }
        float4 frag(VertexOutput i) : SV_Target {
            i.normalDir = normalize(i.normalDir);
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            float3 normalDirection = i.normalDir;
            UnityMetaInput o;
            UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
            
            o.Emission = 0;
            
            float4 _Sand_var = tex2D(_Sand,TRANSFORM_TEX(i.uv0, _Sand));
            float4 _Albedo_var = tex2D(_Albedo,TRANSFORM_TEX(i.uv0, _Albedo));
            float node_6731 = saturate((((1.0 - normalDirection.g)-_Slope)*_Hardness));
            float3 diffColor = lerp((_SandColor.rgb*_Sand_var.rgb),(_DiffColor.rgb*_Albedo_var.rgb),node_6731);
            float specularMonochrome;
            float3 specColor;
            float4 _MetalRRoughGEmissiveBAOA_var = tex2D(_MetalRRoughGEmissiveBAOA,TRANSFORM_TEX(i.uv0, _MetalRRoughGEmissiveBAOA));
            diffColor = DiffuseAndSpecularFromMetallic( diffColor, _MetalRRoughGEmissiveBAOA_var.r, specColor, specularMonochrome );
            float node_8459 = (_Roughness_STR*_MetalRRoughGEmissiveBAOA_var.g);
            float roughness = node_8459;
            o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
            
            return UnityMetaFragment( o );
        }
        ENDCG
    }
}
FallBack "Diffuse"
CustomEditor "ShaderForgeMaterialInspector"
}
